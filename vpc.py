import boto3

# Create an EC2 resource object
ec2_resource = boto3.resource('ec2', region_name='us-east-1')

# Define the parameters for your VPC
vpc_cidr_block = '10.0.0.0/16'  # The IP range for your VPC
vpc_name = 'MyVPC'  # A name for your VPC

# Create the VPC
vpc = ec2_resource.create_vpc(CidrBlock=vpc_cidr_block)

# Add a name tag to the VPC
vpc.create_tags(Tags=[{'Key': 'Name', 'Value': vpc_name}])

# Enable DNS support and hostname for the VPC
vpc.modify_attribute(EnableDnsSupport={'Value': True})
vpc.modify_attribute(EnableDnsHostnames={'Value': True})
vpc_id = vpc.id
subnet_cidr_block_1 = '10.0.1.0/24'  # The IP range for subnet 1
subnet_cidr_block_2 = '10.0.2.0/24'  # The IP range for subnet 2
availability_zone_1 = 'us-east-1a'  # The Availability Zone for subnet 1
availability_zone_2 = 'us-east-1b'  # The Availability Zone for subnet 2
subnet_name_1 = 'Subnet1'  # A name for subnet 1
subnet_name_2 = 'Subnet2'  # A name for subnet 2

# Create the subnets
subnet1 = ec2_resource.create_subnet(VpcId=vpc_id, CidrBlock=subnet_cidr_block_1, AvailabilityZone=availability_zone_1)
subnet2 = ec2_resource.create_subnet(VpcId=vpc_id, CidrBlock=subnet_cidr_block_2, AvailabilityZone=availability_zone_2)
subnet1.create_tags(Tags=[{'Key': 'Name', 'Value': subnet_name_1}])
subnet2.create_tags(Tags=[{'Key': 'Name', 'Value': subnet_name_2}])
print('VPC ID:', vpc_id)
print('subnet1 ID:', subnet1.id)

