import boto3
import schedule
import time

# Create an EC2 client object for the desired region
ec2_client = boto3.client('ec2', region_name='us-east-1')  # Replace 'your-region' with the actual region name

# Function to retrieve instance status
def get_instance_status():
    response = ec2_client.describe_instance_status(IncludeAllInstances=True)
    instance_statuses = response['InstanceStatuses']

    # Process the instance status information
    for instance_status in instance_statuses:
        instance_id = instance_status['InstanceId']
        status = instance_status['InstanceState']['Name']
        print('Instance ID:', instance_id)
        print('Status:', status)
        print('---')

# Schedule the task to run every minute
schedule.every(5).seconds.do(get_instance_status)

# Run the scheduled tasks continuously
while True:
    schedule.run_pending()
    time.sleep(1)
