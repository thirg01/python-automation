import boto3

# Create an EC2 resource object for the desired region
ec2_client = boto3.client('ec2', region_name='us-east-1')  # Replace 'your-region' with the actual region name

statuses = ec2_client.describe_instance_status()
    # Iterate over the instance status information
for status in statuses['InstanceStatuses']:
    ins_status = status['InstanceStatus']['Status']
    sys_status = status['SystemStatus']['Status']
    state = status["InstanceState"]['Name']
    print(f"instance{status['InstanceId']} status is {ins_status} and the system status is {sys_status} the state is {state}")


