import boto3

# List of region and instance IDs
regions_instances = [
    {'region': 'us-east-1', 'instance_ids': ['i-085dff8065c50a908', 'i-0ccbb3886011dbcfa']},
    {'region': 'ap-south-1', 'instance_ids': ['i-0fd213c50f2effa55']}
]
# Replace 'instance-id-1', 'instance-id-2', etc., with the actual instance IDs for each region

# Function to add tags to instances
def add_tags_to_instances(region, instance_ids, tags):
    ec2_client = boto3.client('ec2', region_name=region)
    response = ec2_client.create_tags(
        Resources=instance_ids,
        Tags=tags
    )
    print(f'Tags added to instances in {region} region successfully.')

# Add tags to instances in different regions
for region_instances in regions_instances:
    region = region_instances['region']
    instance_ids = region_instances['instance_ids']

    tags = [
        {'Key': 'Environment', 'Value': 'Production' if region == 'us-east-1' else 'Development'},
        {'Key': 'Department', 'Value': 'IT' if region == 'us-east-1' else 'systemadmin'}
    ]

    add_tags_to_instances(region, instance_ids, tags)
