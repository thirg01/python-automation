import requests
import smtplib
import os
import paramiko
import boto3
import time
import schedule


EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS')
EMAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD')
#rivrohnlcbebxcqd



def restart_server_and_container():
    # Restart EC2 instance
    print('Rebooting the server...')
    ec2_client = boto3.client('ec2', region_name='us-east-1')

    instance_id = 'i-001d7b678b6c51e15'  # Replace with your instance ID
    response = ec2_client.reboot_instances(InstanceIds=[instance_id])

    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        print('Server reboot initiated successfully')
    else:
        print('Failed to initiate server reboot')

    # Wait for the instance to be in a running state
    waiter = ec2_client.get_waiter('instance_running')
    waiter.wait(InstanceIds=[instance_id])

    # Restart the container on the instance
    print('Restarting the container...')
    # Replace the following lines with your code to restart the container on the EC2 instance
    # Use SSH or any other method to connect to the instance and execute the necessary commands
    # Example: ssh into the instance and execute a command to restart the container
    # ...

    print('Container restarted successfully')

# Call the function to restart the server and container
restart_server_and_container()


def send_notification(email_msg):
    print('Sending an email...')
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        message = f"Subject: SITE DOWN\n{email_msg}"
        smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, message)
def monitor_application():
 try:
   response = requests.get('http://ec2-44-201-120-166.compute-1.amazonaws.com:8080//')
   if response.status_code == 200:
       print('Application is running successfully!')
   else:
       print('Application Down. Fix it!')
       msg = f'Application returned {response.status_code}'
       send_notification(msg)

       print('Restarting the application...')
       ssh = paramiko.SSHClient()
       ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
       ssh.connect(hostname='44.201.120.166', username='ubuntu', key_filename='dev-key.pem')
       stdin, stdout, stderr = ssh.exec_command('docker start 9a14eb49e0c2')
       print(stdout.readlines())
       ssh.close()

 except Exception as ex:
        print(f'Connection error happened: {ex}')
        msg = 'Application not accessible at all'
        send_notification(msg)
        restart_server_and_container()

schedule.every(5).minutes.do(monitor_application)

while True:
    schedule.run_pending()



